# Sample-React-Project
## React Native Web App with TypeScript and WebPack
```
ReactNative와 TypeScript, WebPack를 사용하여
React, TypeScript를 활용한 Android, IOS, WEB 개발 환경을 구성한다.
```
## 1. React 필요 설치 패키지 방법
### 1.1 NPM Install
>**[CMD]** npm install -g yarn   
>**[CMD]** npm install -g react-native-cli   
>VSCode에서 @id:vscjava.vscode-java-pack(Java Extension Pack) 플러그인을 설치한다.   
### 1.2 Android SDK 설정
>1.2.1. 안드로이드 스튜디오 설치 (https://developer.android.com/studio)   
>1.2.2. 안드로이드 SDK 설치
>1.2.3. 안드로이드 SDK 패스 설정
> 1.2.3.1 시스템 > 고급 시스템 설정 > 환경변수 > 시스템 변수 > 추가   
>> 변수 이름 : ANDROID_SDK_ROOT / 변수 값 : SDK Path   
>> 기본 패스 : C:\Users\계정명\AppData\Local\Android\Sdk   

> 1.2.3.2 Path > 새로만들기   
>> %ANDROID_SDK_ROOT%\tools   
>> %ANDROID_SDK_ROOT%\platform-tools   
### 1.3 Android Emulator에서 실행 시켰을 시
>1.3.1 Android Studio에서 에뮬레이터 다운로드   
>>실행시 VT-x is disabled in BIOS 오류시 https://ehdtjq0411.tistory.com/4 참고

## 2. Project 생성 방법
>2.1. **[CMD]** react-native init ProjectName   
>> App 패키지명을 등록 할 시   
>> **[CMD]** react-native init ProjectName --package=kr.jsnc.project.app
  
***
```
이하 내용은 새로 생성한 프로젝트 폴더에서 모든것을 진행
```

## 3. 타입스크립트 개발을 위한 환경 구성
>3.1. **[CMD]** yarn add --dev react-native-typescript-transformer typescript   
>3.2. 프로젝트 Root 폴더에 tsconfig.json 파일을 생성한 후 아래 내용을 붙여넣는다.   
```
{
  "compilerOptions": {
    /* Basic Options */
    "target": "esnext" /* Specify ECMAScript target version: 'ES3' (default), 'ES5', 'ES2015', 'ES2016', 'ES2017','ES2018' or 'ESNEXT'. */,
    "module": "commonjs" /* Specify module code generation: 'none', 'commonjs', 'amd', 'system', 'umd', 'es2015', or 'ESNext'. */,
    "lib": [
      "esnext",
      "dom"
    ] /* Specify library files to be included in the compilation. */,
    // "allowJs": true,                       /* Allow javascript files to be compiled. */
    // "checkJs": true,                       /* Report errors in .js files. */
    "jsx": "react" /* Specify JSX code generation: 'preserve', 'react-native', or 'react'. */,
    // "declaration": true,                   /* Generates corresponding '.d.ts' file. */
    // "declarationMap": true,                /* Generates a sourcemap for each corresponding '.d.ts' file. */
    // "sourceMap": true,                     /* Generates corresponding '.map' file. */
    // "outFile": "./",                       /* Concatenate and emit output to single file. */
    // "outDir": "./dist/",                   /* Redirect output structure to the directory. */
    // "rootDir": "./",                       /* Specify the root directory of input files. Use to control the output directory structure with --outDir. */
    // "composite": true,                     /* Enable project compilation */
    // "removeComments": true,                /* Do not emit comments to output. */
    "noEmit": false /* Do not emit outputs. */,
    // "importHelpers": true                  /* Import emit helpers from 'tslib'. */,
    // "downlevelIteration": true,            /* Provide full support for iterables in 'for-of', spread, and destructuring when targeting 'ES5' or 'ES3'. */
    // "isolatedModules": true,               /* Transpile each file as a separate module (similar to 'ts.transpileModule'). */
    /* Strict Type-Checking Options */
    "strict": true /* Enable all strict type-checking options. */,
    // "noImplicitAny": true,                 /* Raise error on expressions and declarations with an implied 'any' type. */
    // "strictNullChecks": true,              /* Enable strict null checks. */
    // "strictFunctionTypes": true,           /* Enable strict checking of function types. */
    // "strictBindCallApply": true,           /* Enable strict 'bind', 'call', and 'apply' methods on functions. */
    // "strictPropertyInitialization": true,  /* Enable strict checking of property initialization in classes. */
    // "noImplicitThis": true,                /* Raise error on 'this' expressions with an implied 'any' type. */
    // "alwaysStrict": true,                  /* Parse in strict mode and emit "use strict" for each source file. */
    /* Additional Checks */
    "noUnusedLocals": true /* Report errors on unused locals. */,
    "noUnusedParameters": true /* Report errors on unused parameters. */,
    // "noImplicitReturns": true,             /* Report error when not all code paths in function return a value. */
    // "noFallthroughCasesInSwitch": true,    /* Report errors for fallthrough cases in switch statement. */
    /* Module Resolution Options */
    "moduleResolution": "node" /* Specify module resolution strategy: 'node' (Node.js) or 'classic' (TypeScript pre-1.6). */,
    // "baseUrl": "./",                       /* Base directory to resolve non-absolute module names. */
    // "paths": {},                           /* A series of entries which re-map imports to lookup locations relative to the 'baseUrl'. */
    // "rootDirs": [],                        /* List of root folders whose combined content represents the structure of the project at runtime. */
    // "typeRoots": [],                       /* List of folders to include type definitions from. */
    // "types": [],                           /* Type declaration files to be included in compilation. */
    // "allowSyntheticDefaultImports": true   /* Allow default imports from modules with no default export. This does not affect code emit, just typechecking. */,
    "esModuleInterop": true /* Enables emit interoperability between CommonJS and ES Modules via creation of namespace objects for all imports. Implies 'allowSyntheticDefaultImports'. */,
    // "preserveSymlinks": true,              /* Do not resolve the real path of symlinks. */
    /* Source Map Options */
    // "sourceRoot": "",                      /* Specify the location where debugger should locate TypeScript files instead of source locations. */
    // "mapRoot": "",                         /* Specify the location where debugger should locate map files instead of generated locations. */
    // "inlineSourceMap": true,               /* Emit a single file with source maps instead of having a separate file. */
    // "inlineSources": true,                 /* Emit the source alongside the sourcemaps within a single file; requires '--inlineSourceMap' or '--sourceMap' to be set. */
    /* Experimental Options */
    // "experimentalDecorators": true,        /* Enables experimental support for ES7 decorators. */
    // "emitDecoratorMetadata": true,         /* Enables experimental support for emitting type metadata for decorators. */
    "skipLibCheck": true,
    "forceConsistentCasingInFileNames": true,
    "resolveJsonModule": true
  },
  "exclude": ["node_modules"]
}
```
>3.3. 프로젝트 Root 폴더에 tslint.json 파일을 추가 생성 후 아래 내용을 붙여넣는다.
```
{
  "extends": ["tslint-react-recommended"],
  "rules": {
    "ordered-imports": [true],
    "quotemark": [true, "single", "jsx-single", "avoid-escape"],
    "semicolon": [true, "never"],
    "member-access": [false],
    "member-ordering": [false],
    "trailing-comma": [
      true,
      {
        "singleline": "never",
        "multiline": "always"
      }
    ],
    "no-empty": false,
    "no-submodule-imports": false,
    "no-implicit-dependencies": false,
    "no-constant-condition": false,
    "triple-equals": [true, "allow-undefined-check"],
    "ter-indent": [
      true,
      2,
      {
        "SwitchCase": 1
      }
    ],
    "no-duplicate-imports": false,
    "jsx-alignment": true,
    "jsx-no-bind": true,
    "jsx-no-lambda": true,
    "max-classes-per-file": [false]
  }
}
```
>3.4. index.js를 index.ts로 변경 후 아래 내용으로 변경한다   
>> **[CMD]** ren index.js index.ts   
```
import {AppRegistry} from 'react-native';
import {name as appName} from './app.json';
import App from './app/App';

AppRegistry.registerComponent(appName, () => App);
```
>3.5. metro.config.js를 아래 내용으로 변경한다.
```
module.exports = {
  transformer: {
    babelTransformerPath: require.resolve('react-native-typescript-transformer'),
    getTransformOptions: async () => ({
      transform: {
        experimentalImportSupport: false,
        inlineRequires: false,
      },
    }),
  },
};
```

>3.6. app폴더를 생성한 한 후 App.js 파일을 App.tsx로 확장자를 변경한후 app폴더로 이동한다.   
> **[CMD]** md app   
> **[CMD]** ren App.js App.tsx   
> **[CMD]** move App.tsx app   

>3.7. App.tsx 파일을 아래 내용으로 수정한다.  
**[코드변경이유는 기본 코드가 Device 코드로 개발되기 때문에 Web에서는 작동하지 않는다.]**
```
import * as React from 'react'
import {
  Animated,
  Dimensions,
  Text,
  TouchableWithoutFeedback,
  View,
} from 'react-native'

const {height} = Dimensions.get('window')

const AnimatedView = Animated.createAnimatedComponent(View)

const App: React.FC<any> = (props: any) => {
  const {} = props
  const opacity: Animated.Value = new Animated.Value(1)

  const animate = () => {
    Animated.sequence([
      Animated.timing(opacity, {
        toValue: 0.4,
        duration: 250,
      }),
      Animated.timing(opacity, {
        toValue: 1,
        duration: 250,
      }),
    ]).start()
  }

  return (
    <View style={{height: height}}>
      <TouchableWithoutFeedback onPress={animate}>
        <AnimatedView
          style={{
            height,
            flex: 1,
            justifyContent: 'center',
            alignItems: 'center',
            opacity: opacity,
          }}>
          <Text>Hello, World!!!!</Text>
        </AnimatedView>
      </TouchableWithoutFeedback>
    </View>
  )
}

export default App

```

## 4. React-Dom & React Native Web & react-scripts 설치
> 4.1. **[CMD]** yarn add react-dom react-native-web react-scripts   
> 4.2. **[CMD]** yarn add --dev @types/react-dom @types/react @types/react-native    

## 5. Webpack 및 플러그인 설치
>> **[CMD]** yarn add --dev webpack webpack-cli webpack-dev-server   
>> **[CMD]** yarn add --dev html-webpack-plugin   
>> **[CMD]** yarn add --dev ts-loader fork-ts-checker-webpack-plugin source-map-loader   

## 6. babel 및 플러그인 설치
>> **[CMD]** yarn add --dev babel-loader babel-core babel-preset-react    
>> [babel 참고사이트](https://velog.io/@pop8682/%EB%B2%88%EC%97%AD-ES6-babel-%EA%B7%B8%EB%A6%AC%EA%B3%A0-webpack%EC%9D%84-%EC%96%B4%EB%96%BB%EA%B2%8C-%EC%82%AC%EC%9A%A9%ED%95%A0%EA%B9%8C-3dk03jhwqx)


## 7. Web 개발 구성
> 7.1. web 폴더 생성 **[CMD]** md web   
> 7.2. web 폴더 내 index.html 파일 생성 및 아래의 내용 저장   
```
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta http-equiv="X-UA-Compatible" content="ie=edge" />
    <title>SampleReactProject</title>
  </head>
  <body>
    <div id="app-root"></div>
  </body>
</html>
```

> 7.3 webpack.config.js 파일을 web 폴더에 추가 및 아래의 내용 저장
```
const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const RULES = require('./webpack.rules')
const rootDir = path.join(__dirname, '..')
const webpackEnv = process.env.NODE_ENV || 'development'

module.exports = {
  mode: webpackEnv,
  entry: {
    app: path.join(rootDir, './index.web.ts'),
  },
  output: {
    path: path.resolve(rootDir, 'dist'),
    filename: 'app-[hash].bundle.js',
  },
  devtool: 'source-map',
  module: {
    rules: RULES,
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: path.join(__dirname, './index.html'),
    }),
    new webpack.HotModuleReplacementPlugin(),
  ],
  resolve: {
    extensions: [
      '.web.tsx',
      '.web.ts',
      '.tsx',
      '.ts',
      '.web.jsx',
      '.web.js',
      '.jsx',
      '.js',
    ],
    alias: Object.assign({
      'react-native$': 'react-native-web',
    }),
  },
}

```
> 7.4 webpack.rules.js 파일을 web 폴더에 추가 및 아래의 내용 저장
```
module.exports = [
  {
    test: /\.(tsx|ts|jsx|js|mjs)$/,
    exclude: /node_modules/,
    loader: 'ts-loader',
  },
]

```

> 7.5. 프로젝트 Root에 index.web.ts 파일을 추가한 후 아래 내용을 입력   
```
import {AppRegistry} from 'react-native'
import {name as appName} from './app.json'
import App from './app/App'

AppRegistry.registerComponent(appName, () => App)

AppRegistry.runApplication(appName, {
  initialProps: {},
  rootTag: document.getElementById('app-root'),
})

```
> 7.6. 프로젝트 폴더의 package.json에 웹(실행&빌드)환경 명령어 추가
```
{
  scripts : {
    "web": "cd web && webpack-dev-server --open",
    "build:web": "cd web && webpack"
  }
}
```

## 8. 프로젝트 실행 및 웹 빌드 방법
> **[CMD]** yarn ios   
> **[CMD]** yarn android   
> **[CMD]** yarn web   
> **[CMD]** yarn build:web   

>>### 8.1 Android Device에서 USB를 통해 실행 시 Could not connect to development server 오류
>> **[CMD]** adb devices 로 디바이스 확인   
>> **[CMD]** adb -s <device name> reverse tcp:8081 tcp:8081   

## 9. webpack-dev-server 포트 변경하기
> webpack.config.js 에 내용추가
```
module.exports = {
  devServer: {
    contentBase: path.join(__dirname, 'dist'),
    compress: true,
    port: 3000
  },
}
```

## License
The MIT License.
Copyright ⓒ 2020 JSNC inc. all rights reserved
